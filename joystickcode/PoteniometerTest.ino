
int pinY = A2;
int pinX = A4;

const int CENTER_X = 540;
const int CENTER_Y = 410;
const int OFFSET = 50;

int x = 0;
int y=0;

void setup()
{
 pinMode(pinX, INPUT);
 pinMode(pinY, INPUT);

 Serial.begin(9600); //This pipes to the serial monitor
 Serial1.begin(9600); //This is the UART, pipes to sensors attached to board
}

void loop()
{
  
 x = analogRead(pinX);
 y = analogRead(pinY);

 if(x < CENTER_X - OFFSET) // Going left
 {
   Serial1.print('a');
   
  if(y < CENTER_Y - OFFSET) // Going down
     Serial1.print('s');
     
   if(y > CENTER_Y + OFFSET) // Going up
     Serial1.print('w');
 } 
 else if( x > CENTER_X + OFFSET) // Going right
 {
   Serial1.print('d');
   
     if(y < CENTER_Y - OFFSET) // Going down
       Serial1.print('s');
     
     if(y > CENTER_Y + OFFSET) // Going up
     Serial1.print('w');
 }
 else
 {
   if(y < CENTER_Y - OFFSET) // Going down
     Serial1.print('s');
     
   else if(y > CENTER_Y + OFFSET) // Going up
     Serial1.print('w'); 
   
   else
     Serial1.print('r');
 }
 
 Serial1.println();
 
// Serial.print("X ");
 //Serial.println(x);
 //Serial.print("Y ");
 //Serial.println(y);
 
 //Serial.println("Hello");
 //Serial1.println("Hello World");
 //x = analogRead(pinX);
 //String v = "X: " + a;
 //Serial1.print("X: ");
 //Serial1.println('a');
 
 delay(500);              // wait for a second
}

