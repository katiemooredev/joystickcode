
void setup() 
{
  Serial1.begin(9600);          //initialize xbee serial
  Serial.begin(9600);    	//initialize serial
  
  Keyboard.begin();
}

void loop() 
{
  while(Serial1.available())
  {  
        //is there anything to read?
	char getData = Serial1.read();  //if yes, read it

        Serial.println(getData);

	if(getData == 'a')
        { 
          Serial.println("Type a");
  	  Keyboard.write('a');
        }
        
        else if(getData == 'd')
        {
          Serial.println("Type d");
  	  Keyboard.write('d');
        }
        
        else if(getData == 'w')
        {
          Serial.println("Type w");
          Keyboard.write('w');
        }
        
        else if(getData == 's')
        {
          Serial.println("Type s");
          Keyboard.write('s');
        }
          
        else if(getData == 'r')
        {
          Serial.println("Release Keys");
          Keyboard.releaseAll();
        }
          
        else if(getData == 'h')
        {
          Serial.println("Release Keyboard");
          Keyboard.end();
        }  
          
        else
        {
          Serial.println("Release all Keys");
          Keyboard.releaseAll();
        }
          
        delay(250);
  }
}
